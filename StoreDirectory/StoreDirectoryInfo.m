//
//  StoreDirectoryInfo.m
//  StoreDirectory
//
//  Created by Alan Orchaton on 11/14/13.
//  Copyright (c) 2013 Alan Orchaton. All rights reserved.
//

#import "StoreDirectoryInfo.h"


@implementation StoreDirectoryInfo

@dynamic store_num;
@dynamic address;
@dynamic city;

@end
