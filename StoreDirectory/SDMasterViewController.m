//
//  SDMasterViewController.m
//  StoreDirectory
//
//  Created by Alan Orchaton on 11/14/13.
//  Copyright (c) 2013 Alan Orchaton. All rights reserved.
//

#import "SDMasterViewController.h"
#import "StoreDirectoryInfo.h"
#import "SDAppDelegate.h"

@interface SDMasterViewController ()

@end

@implementation SDMasterViewController

@synthesize appDelegate = _appDelegate;
@synthesize context = _context;
@synthesize storeDirectoryResults;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(SDAppDelegate *)appDelegate{
    
    if(!_appDelegate){
        _appDelegate = [[UIApplication sharedApplication]delegate];
    }
    return _appDelegate;
}

-(NSManagedObjectContext*)context{
    if(!_context){
        _context = [self.appDelegate managedObjectContext];
        
    }
    return _context;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    SDAppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    _context = [appDelegate managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"StoreDirectoryInfo" inManagedObjectContext:_context];
    [fetchRequest setEntity:entity];
    NSError *error;
    self.storeDirectoryResults = [_context executeFetchRequest:fetchRequest error:&error];
    self.title = @"Store Directory";

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    //Count of the number of rows goes here
    return [storeDirectoryResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"storeDataCell";
    StoreInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    //******Change here for cell data
    StoreDirectoryInfo *info = [storeDirectoryResults objectAtIndex:indexPath.row];
    cell.storeNumber.text = info.store_num;
    cell.address.text = info.address;
    cell.city.text = info.city;
    cell.state.text = info.state;
    cell.brand.text = info.text;
    NSLog(@"Store number %@",  info.store_num);
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@, %@, %@, %@",
                                 info.city, info.address, info.state, info.brand];
    
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
