//
//  StoreInfoTableViewCell.h
//  StoreDirectory
//
//  Created by Lyssa Livingston on 11/15/13.
//  Copyright (c) 2013 Alan Orchaton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreInfoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *storeNumber;

@property (weak, nonatomic) IBOutlet UILabel *address;
@property (weak, nonatomic) IBOutlet UILabel *city;
@property (weak, nonatomic) IBOutlet UILabel *state;
@property (weak, nonatomic) IBOutlet UILabel *brand;

@end
