//
//  SDOpeningViewController.m
//  StoreDirectory
//
//  Created by Alan Orchaton on 11/14/13.
//  Copyright (c) 2013 Alan Orchaton. All rights reserved.
//

#import "SDOpeningViewController.h"

@interface SDOpeningViewController ()
@property (weak, nonatomic) IBOutlet UITextField *storeNumber;
@property (weak, nonatomic) IBOutlet UITextField *storeBrand;
@property (weak, nonatomic) IBOutlet UITextField *storeCity;

@end

@implementation SDOpeningViewController

@synthesize managedObjectContext;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
